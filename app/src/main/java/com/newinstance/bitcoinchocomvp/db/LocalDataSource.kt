package com.newinstance.bitcoinchocomvp.db

import android.app.Application
import com.newinstance.bitcoinchocomvp.mvp.model.ConvertationModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */

open class LocalDataSource(application: Application) {

    val dbDao: ConvertationHistoryDao
    open val allItems: Observable<List<ConvertationModel>>

    init {
        val db = LocalDatabase.getInstance(application)
        dbDao = db.dbDao()
        allItems = dbDao.all
    }


    fun insert(item:ConvertationModel) {
        Completable.fromAction {
            dbDao.insert(item)
        }.subscribeOn(Schedulers.io()).subscribe()
    }

    fun delete(item: ConvertationModel) {
        Completable.fromAction {
            dbDao.delete(item.id)
        }.subscribeOn(Schedulers.io()).subscribe()
    }

}