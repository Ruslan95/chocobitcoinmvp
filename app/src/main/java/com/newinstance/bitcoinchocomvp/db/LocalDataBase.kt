package com.newinstance.bitcoinchocomvp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.newinstance.bitcoinchocomvp.mvp.model.ConvertationModel

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@Database(entities = [ConvertationModel::class], version = 1)
abstract class LocalDatabase : RoomDatabase() {

    //dao
    abstract fun dbDao(): ConvertationHistoryDao

    companion object {

        //thread safety singleton
        @Volatile
        private var INSTANCE: LocalDatabase? = null

        fun getInstance(context: Context): LocalDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            //thread safety checking
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    LocalDatabase::class.java,
                    "bitcoin_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}