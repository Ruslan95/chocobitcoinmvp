package com.newinstance.bitcoinchocomvp.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.newinstance.bitcoinchocomvp.mvp.model.ConvertationModel
import io.reactivex.Observable

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@Dao
interface ConvertationHistoryDao{
    @get:Query("SELECT * FROM convertation_table")
    val all: Observable<List<ConvertationModel>>

    @Query("SELECT * FROM convertation_table")
    fun allPaged(): DataSource.Factory<Int,ConvertationModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: ConvertationModel)

    @Query("DELETE FROM convertation_table WHERE id = :id")
    fun delete(id: Int?)

    @Query("DELETE FROM convertation_table")
    fun deleteAll()
}