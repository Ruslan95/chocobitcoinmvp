package com.newinstance.bitcoinchocomvp.adapters.baseAdapterArc

import android.view.View

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */

interface BaseAdapterCallBack<I> {
    fun onItemClickListener(item:I,position:Int)
    fun onItemViewClickListener(item:I,position: Int,view: View)
}