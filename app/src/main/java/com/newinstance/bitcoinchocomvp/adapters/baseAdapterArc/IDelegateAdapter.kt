package com.newinstance.bitcoinchocomvp.adapters.baseAdapterArc

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
interface IDelegateAdapter <VH:RecyclerView.ViewHolder, I>{
    fun onCreateViewHolder(parent: ViewGroup,
                           viewType: Int):RecyclerView.ViewHolder
    fun onBindViewHolder(holder: VH, position: Int)
    fun onRecycled(holder:VH)
    fun isForViewType(items: List<*>,position: Int):Boolean
}