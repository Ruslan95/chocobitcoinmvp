package com.newinstance.bitcoinchocomvp.adapters.appAdapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.newinstance.bitcoinchocomvp.R
import com.newinstance.bitcoinchocomvp.mvp.model.ConvertationModel
import kotlinx.android.synthetic.main.row_item_convertation.view.*
import java.text.NumberFormat

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */

class ConvertationsAdapter : PagedListAdapter<ConvertationModel,
        ConvertationsAdapter.ConvertationViewHolder>(
    DiffUtilCallBack
){

    companion object {
        private val DiffUtilCallBack = object :
            DiffUtil.ItemCallback<ConvertationModel>() {
            // Concert details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(oldItem: ConvertationModel,
                                         newItem: ConvertationModel) = oldItem.id == newItem.id

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: ConvertationModel,
                                            newItem: ConvertationModel) = oldItem == newItem
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConvertationViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_item_convertation, parent, false)
        return ConvertationViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ConvertationViewHolder, position: Int) {
        val currentItem : ConvertationModel? = getItem(position)
        currentItem?.let {
            holder.bind(it)
        }
    }

    inner class ConvertationViewHolder(view: View):RecyclerView.ViewHolder(view){
        fun bind(item : ConvertationModel?){
            itemView.row_card_amount.text = NumberFormat.getInstance().format(item!!.amount)
            itemView.row_card_btc_amount.text = NumberFormat.getInstance().format(item.btc_amount)
            itemView.row_card_currency.text = item.currency
            itemView.row_card_date.text = item.date
            itemView.row_card_id.text = "Id: ${item.id}"
            when(item.currency) {
                "EUR" -> {
                    itemView.row_card_currency_flag.setImageResource(R.drawable.ic_european_flag)
                }
                "KZT" -> {
                    itemView.row_card_currency_flag.setImageResource(R.drawable.ic_kazakhstan_flag)
                }
                "USD" -> {
                    itemView.row_card_currency_flag.setImageResource(R.drawable.ic_usa_flag)
                }
            }
        }
    }
}