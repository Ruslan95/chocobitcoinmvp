package com.newinstance.bitcoinchocomvp.adapters.baseAdapterArc

import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */

abstract class BaseAdapter<I>: RecyclerView.Adapter<BaseViewHolder<I>>() {
    protected var mDataList: List<I> = ArrayList<I>()
    protected var mCallBack: BaseAdapterCallBack<I>? = null
    var hasItems = false

    fun addCallBack(callback: BaseAdapterCallBack<I>){
        this.mCallBack = callback
    }

    fun removeCallBack(){
        this.mCallBack = null
    }

    fun setList(dataList: List<I>){
        (this.mDataList as ArrayList).addAll(dataList)
        hasItems = true
        notifyDataSetChanged()
    }

    fun addItem(item:I){
        (this.mDataList as ArrayList).add(item)
        notifyItemChanged(this.mDataList.size-1)
    }

    fun addItemToTop(item:I){
        (this.mDataList as ArrayList).add(0,item)
        notifyItemChanged(0)
    }

    fun updateItems(dataList:List<I>){
        (this.mDataList as ArrayList).clear()
        setList(dataList)
    }

    fun clearItems(){
        (this.mDataList as ArrayList).clear()
        hasItems = false
        notifyDataSetChanged()
    }

    fun getItems():List<I>{
        return mDataList
    }


    override fun onBindViewHolder(holder: BaseViewHolder<I>, position:Int){
        holder.bind(mDataList[position])

        holder.itemView.setOnClickListener{
            mCallBack?.onItemClickListener(mDataList[position],position)
        }

    }

    override fun getItemCount(): Int {
        return mDataList.count()
    }
}