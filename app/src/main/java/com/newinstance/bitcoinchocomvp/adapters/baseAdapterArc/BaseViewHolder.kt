package com.newinstance.bitcoinchocomvp.adapters.baseAdapterArc

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
abstract class BaseViewHolder <I> (itemView: View):RecyclerView.ViewHolder(itemView){
    abstract fun bind(item:I)
}