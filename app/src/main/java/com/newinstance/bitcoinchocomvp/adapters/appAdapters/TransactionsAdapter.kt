package com.newinstance.bitcoinchocomvp.adapters.appAdapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.newinstance.bitcoinchocomvp.R
import com.newinstance.bitcoinchocomvp.adapters.baseAdapterArc.BaseAdapter
import com.newinstance.bitcoinchocomvp.adapters.baseAdapterArc.BaseViewHolder
import com.newinstance.bitcoinchocomvp.mvp.model.TransactionsModel
import com.newinstance.bitcoinchocomvp.utils.CommonUtils
import kotlinx.android.synthetic.main.row_item_trasaction_buy.view.*
import kotlinx.android.synthetic.main.row_item_trasaction_sell.view.*
import java.lang.IllegalStateException
import java.text.NumberFormat

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */

class TransactionsAdapter: BaseAdapter<TransactionsModel>(){

    override fun getItemViewType(position: Int): Int  = mDataList[position].type

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<TransactionsModel> {
        when(viewType) {
            1 -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_item_trasaction_buy, parent, false)
                    return BuyViewHolder(itemView)
            }
            0 -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_item_trasaction_sell, parent, false)
                return SellViewHolder(itemView)
            }
            else -> {
                throw IllegalStateException()
            }
        }
    }
    inner class BuyViewHolder(itemView: View):
        BaseViewHolder<TransactionsModel>(itemView){
        override fun bind(item: TransactionsModel) {
            itemView.row_item_transaction_buy_price.text = NumberFormat.getInstance().format(item.price.toFloat())+ " $"
            itemView.row_item_transaction_buy_amount.text = NumberFormat.getInstance().format(item.amount.toFloat())
            itemView.row_item_transaction_buy_date.text = CommonUtils.convertTimeInMillisToDateString(item.date.toLong())
        }

    }
    inner class SellViewHolder(itemView: View):
        BaseViewHolder<TransactionsModel>(itemView){
        override fun bind(item: TransactionsModel) {

            itemView.row_item_transaction_sell_price.text = NumberFormat.getInstance().format(item.price.toFloat()) + " $"
            itemView.row_item_transaction_sell_amount.text = NumberFormat.getInstance().format(item.amount.toFloat())
            itemView.row_item_transaction_sell_date.text = CommonUtils.convertTimeInMillisToDateString(item.date.toLong())

        }

    }

}