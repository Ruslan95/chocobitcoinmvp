package com.newinstance.bitcoinchocomvp.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
object CommonUtils {
    @SuppressLint("SimpleDateFormat")
    fun formatISO_8601(time: String?,timePatter: String = Constants.FORMAT_DATE): String {

        val isoFormat = SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault())
        val calendar = Calendar.getInstance()
        calendar.timeZone = TimeZone.getTimeZone("UTC")
        calendar.time = isoFormat.parse(time)

        val mCalendar = GregorianCalendar()
        val mTimeZone = mCalendar.timeZone
        val mGMTOffset = mTimeZone.rawOffset.toLong()
        val offsetHour = TimeUnit.HOURS.convert(mGMTOffset, TimeUnit.MILLISECONDS).toInt()

        return SimpleDateFormat(timePatter).format(calendar.time) ?: "Нет данных"
    }

    @SuppressLint("SimpleDateFormat")
    fun formatISO_8601_typed(time: String?,datyType:Int,timePatter: String = Constants.FORMAT_DATE): Int {

        val isoFormat = SimpleDateFormat( "yyyy-MM-dd", Locale.getDefault())
        val calendar = Calendar.getInstance()
        calendar.timeZone = TimeZone.getTimeZone("UTC")
        calendar.time = isoFormat.parse(time)

        return calendar.get(datyType)

    }

    fun convertTimeInMillisToDateString(timeInMillis: Long, timePatter: String = Constants.FORMAT_DATE): String {
        val date = Date(timeInMillis * 1000L)
        val sdf = SimpleDateFormat(timePatter)
        val formattedDate = sdf.format(date)
        return formattedDate
    }
}