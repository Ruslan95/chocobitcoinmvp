package com.newinstance.bitcoinchocomvp.utils

object Constants {
    const val ICON_ATTRIBUTE = "icon"
    const val TITLE_ATTRIBUTE = "title"
    const val ITEM_TAG = "item"
    const val ID_ATTRIBUTE = "id"

    const val FORMAT_DATE= "d MMMM yyyy в HH:mm"
    const val FORMAT_DATE_WHITOUT_TIME= "yyyy-MM-dd"
}