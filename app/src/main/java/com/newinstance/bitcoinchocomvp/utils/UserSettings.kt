package com.newinstance.bitcoinchocomvp.utils

import com.criations.bulldog_annotations.Bulldog

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */

@Bulldog(name = "UserPref")
object User {
    const val currency:String = "KZT"
}