package com.newinstance.bitcoinchocomvp.di

import android.app.Application
import android.content.Context
import com.newinstance.bitcoinchocomvp.db.LocalDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */

@Module
class AppModule(private val app:Application){

    @CustomAppScope
    @Provides
    fun provideContext():Context = app

    @CustomAppScope
    @Provides
    fun provideDataSource() = LocalDataSource(app)
}