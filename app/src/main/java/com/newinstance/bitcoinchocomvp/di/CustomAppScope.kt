package com.newinstance.bitcoinchocomvp.di

import javax.inject.Scope
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
annotation class CustomAppScope