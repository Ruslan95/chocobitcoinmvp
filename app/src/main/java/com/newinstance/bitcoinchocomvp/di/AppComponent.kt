package com.newinstance.bitcoinchocomvp.di

import android.content.Context
import com.newinstance.bitcoinchocomvp.BitcoinApp
import com.newinstance.bitcoinchocomvp.api.BitcoinApi
import com.newinstance.bitcoinchocomvp.api.TransactionsApi
import com.newinstance.bitcoinchocomvp.db.LocalDataSource
import com.newinstance.bitcoinchocomvp.mvp.presenters.ConverterPresenter
import dagger.Component
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */
@CustomAppScope
@Singleton
@Component(modules = [AppModule::class,RemoteModule::class])
interface AppComponent {
    fun getContext(): Context
    fun getLocalDatasource() : LocalDataSource
    fun getApi(): BitcoinApi
    fun getAnotherApi(): TransactionsApi

}