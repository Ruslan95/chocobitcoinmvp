package com.newinstance.bitcoinchocomvp.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.newinstance.bitcoinchocomvp.api.BitcoinApi
import com.newinstance.bitcoinchocomvp.api.TransactionsApi
import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */
@Module
class RemoteModule {

    private var urls:ArrayList<String>

    constructor(vararg urls: String){
        this.urls = urls.toList() as ArrayList<String>
    }

    @CustomAppScope
    @Provides
    fun provideGson():Gson =
        GsonBuilder().create()

    @CustomAppScope
    @Provides
    fun provideOkHttpClient():OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()

    @BitcoinApis
    @CustomAppScope
    @Provides
    fun provideRetrofit(gson: Gson,okHttpClient: OkHttpClient): Retrofit =
       Retrofit.Builder()
           .addConverterFactory(GsonConverterFactory.create(gson))
           .baseUrl(urls[0])
           .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
           .client(okHttpClient)
           .build()

    @TransactionApis
    @CustomAppScope
    @Provides
    fun provideRetrofitAnother(gson: Gson,okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(urls[1])
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()


    @CustomAppScope
    @Provides
    fun getApiService(
        @BitcoinApis retrofit: Retrofit): BitcoinApi {
        return retrofit.create(BitcoinApi::class.java)
    }


    @CustomAppScope
    @Provides
    fun getApiAnotherService(@TransactionApis retrofit: Retrofit): TransactionsApi {
        return retrofit.create(TransactionsApi::class.java)
    }


}