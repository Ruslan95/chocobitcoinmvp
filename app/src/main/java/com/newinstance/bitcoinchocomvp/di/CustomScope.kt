package com.newinstance.bitcoinchocomvp.di

import java.lang.annotation.Documented
import javax.inject.Scope
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Qualifier



/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class CustomScope

@Documented
@Qualifier
annotation class BitcoinApis

@Documented
@Qualifier
annotation class TransactionApis