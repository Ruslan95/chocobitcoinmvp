package com.newinstance.bitcoinchocomvp.di

import com.newinstance.bitcoinchocomvp.mvp.presenters.ConvertationPresenter
import com.newinstance.bitcoinchocomvp.mvp.presenters.ConverterPresenter
import com.newinstance.bitcoinchocomvp.mvp.presenters.RatePresenter
import com.newinstance.bitcoinchocomvp.mvp.presenters.TransactionsPresenter
import com.newinstance.bitcoinchocomvp.mvp.view.fragments.ConverterFragment
import dagger.Component

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@CustomScope
@Component(dependencies = [AppComponent::class])
interface SComponent {
    //Presenters
    fun inject(presenter: ConverterPresenter)
    fun inject(presenter: ConvertationPresenter)
    fun inject(presenter: TransactionsPresenter)
    fun inject(presenter: RatePresenter)

    //Fragment
    fun inject(fragment:ConverterFragment)
}
