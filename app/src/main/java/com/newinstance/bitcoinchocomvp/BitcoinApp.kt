package com.newinstance.bitcoinchocomvp

import android.app.Activity
import android.app.Application
import com.criations.bulldog_runtime.bullDogCtx
import com.newinstance.bitcoinchocomvp.api.BitcoinApi
import com.newinstance.bitcoinchocomvp.api.TransactionsApi
import com.newinstance.bitcoinchocomvp.di.*

/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */

class BitcoinApp : Application() {
    companion object {

        fun get(activity: Activity): BitcoinApp {
            return activity.application as BitcoinApp
        }

        fun getApplicationComponent(): AppComponent {
            return appComponent
        }

        lateinit var appComponent:AppComponent
        lateinit var sComponent: SComponent
    }

    override fun onCreate() {
        super.onCreate()
        bullDogCtx = this
        createAppComponent()
        createSComponent()
    }

    private fun createAppComponent() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .remoteModule(RemoteModule(BitcoinApi.BASE_URL,TransactionsApi.TRANSACTIONS_URL)).build()
    }

    private fun createSComponent(){
        sComponent = DaggerSComponent.builder().appComponent(appComponent).build()
    }


}