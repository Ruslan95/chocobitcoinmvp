package com.newinstance.bitcoinchocomvp.ui.CustomBottomNavBar

import android.content.Context
import android.content.res.XmlResourceParser
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.core.content.ContextCompat
import com.newinstance.bitcoinchocomvp.utils.Constants.ICON_ATTRIBUTE
import com.newinstance.bitcoinchocomvp.utils.Constants.ID_ATTRIBUTE
import com.newinstance.bitcoinchocomvp.utils.Constants.ITEM_TAG
import com.newinstance.bitcoinchocomvp.utils.Constants.TITLE_ATTRIBUTE

/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */

class BottomBarParser (private val context: Context, res: Int) {

    private val parser: XmlResourceParser = context.resources.getXml(res)

    fun parse(): List<BottomBarItem> {
        val items: MutableList<BottomBarItem> = mutableListOf()
        var eventType: Int?

        do {
            eventType = parser.next()

            if (eventType == XmlResourceParser.START_TAG && parser.name == ITEM_TAG)
                items.add(getTabConfig(parser))

        } while (eventType != XmlResourceParser.END_DOCUMENT)

        return items.toList()
    }

    private fun getTabConfig(parser: XmlResourceParser): BottomBarItem {
        val attributeCount = parser.attributeCount
        var itemText: String? = null
        var itemDrawable: Drawable? = null
        var id:Int? = null

        for (i in 0 until attributeCount)
            when (parser.getAttributeName(i)) {
                ICON_ATTRIBUTE -> itemDrawable =
                    ContextCompat.getDrawable(context, parser.getAttributeResourceValue(i, 0))
                TITLE_ATTRIBUTE -> {
                    itemText = try {
                        context.getString(parser.getAttributeResourceValue(i, 0))
                    } catch (e: Exception) {
                        parser.getAttributeValue(i)
                    }
                }
                ID_ATTRIBUTE -> {
                    id = parser.getAttributeResourceValue(i,0)
                }
            }
        Log.wtf("Ids","$id")
        return BottomBarItem(id!!,itemText ?: "", itemDrawable!!, alpha = 0)
    }
}