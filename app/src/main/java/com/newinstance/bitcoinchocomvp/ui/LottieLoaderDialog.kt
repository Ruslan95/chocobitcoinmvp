package com.newinstance.bitcoinchocomvp.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import com.newinstance.bitcoinchocomvp.R

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */

class LottieLoaderDialog: MvpAppCompatDialogFragment(){
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        if (dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        this.isCancelable = false
        val view = inflater.inflate(R.layout.dialog_lottie_loader, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}