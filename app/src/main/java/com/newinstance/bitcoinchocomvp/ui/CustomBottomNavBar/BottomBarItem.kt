package com.newinstance.bitcoinchocomvp.ui.CustomBottomNavBar

import android.graphics.RectF
import android.graphics.drawable.Drawable

/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */

data class BottomBarItem(val id:Int,var title: String, val icon: Drawable, var rect: RectF = RectF(), var alpha: Int)