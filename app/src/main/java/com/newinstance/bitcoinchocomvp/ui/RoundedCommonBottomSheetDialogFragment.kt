package com.newinstance.bitcoinchocomvp.ui

import android.app.Dialog
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.newinstance.bitcoinchocomvp.R

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */

open class RoundedCommonBottomSheetDialogFragment : MvpAppCompatDialogFragment() {

    override fun getTheme(): Int = R.style.BaseBottomSheetDialogCommon

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = BottomSheetDialog(requireContext(), theme)

}