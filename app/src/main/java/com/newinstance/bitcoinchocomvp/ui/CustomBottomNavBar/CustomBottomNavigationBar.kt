package com.newinstance.bitcoinchocomvp.ui.CustomBottomNavBar

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.newinstance.bitcoinchocomvp.R
import kotlin.math.abs

/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */

class CustomBottomNavigationBar: View {
    /**
     * Initial def values
     * */

    private var barBackgroundColor = Color.WHITE
    private var barIndicatorColor = Color.BLACK
    private var barSideMargins = resources.getDimensionPixelOffset(R.dimen.bottom_bar_margins).toFloat()
    private var barItemPadding = resources.getDimensionPixelOffset(R.dimen.bottom_bar_item_padding).toFloat()
    private var itemAnimDuration = 200L

    private var itemIconSize = resources.getDimensionPixelOffset(R.dimen.bottom_bar_item_icon_size).toFloat()
    private var itemIconMargin = resources.getDimensionPixelOffset(R.dimen.bottom_bar_item_icon_margin).toFloat()
    private var itemIconTint = Color.GRAY
    private var itemIconTintActive = Color.BLACK

    private var itemTextColor = Color.BLACK
    private var itemTextSize = resources.getDimension(R.dimen.bottom_bar_item_text_size)
    private var itemFontFamily = 0

    /**
     * Dynamic Values
     * */

    private var itemWidth = 0f
    private var activeItem = 0
    private var currentIconTint = itemIconTintActive
    private var indicatorLocation = barSideMargins

    private var items = listOf<BottomBarItem>()
    var onItemCountSelected: (Int) -> Unit = {}
    var onItemSelected: (BottomBarItem) -> Unit = {}
    var onItemReselected: (Int) -> Unit = {}

    private val rect = RectF()

    private val paintIndicator = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = barIndicatorColor
    }
    private val paintText= Paint().apply {
        isFakeBoldText = true
        isAntiAlias = true
        style = Paint.Style.FILL
        color = itemTextColor
        textSize = itemTextSize
        textAlign = Paint.Align.CENTER
    }

    constructor(context: Context) : super(context)

    //Custom attributeSet bottom bar constructor
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.CustomBottomNavigationBar, 0, 0)
        barBackgroundColor = typedArray.getColor(R.styleable.CustomBottomNavigationBar_backgroundColor, this.barBackgroundColor)
        barIndicatorColor = typedArray.getColor(R.styleable.CustomBottomNavigationBar_indicatorColor, this.barIndicatorColor)
        barSideMargins = typedArray.getDimension(R.styleable.CustomBottomNavigationBar_sideMargins, this.barSideMargins)
        barItemPadding = typedArray.getDimension(R.styleable.CustomBottomNavigationBar_itemPadding, this.barItemPadding)
        itemTextColor = typedArray.getColor(R.styleable.CustomBottomNavigationBar_textColor, this.itemTextColor)
        itemTextSize = typedArray.getDimension(R.styleable.CustomBottomNavigationBar_textSize, this.itemTextSize)
        itemIconSize = typedArray.getDimension(R.styleable.CustomBottomNavigationBar_iconSize, this.itemIconSize)
        itemIconTint = typedArray.getColor(R.styleable.CustomBottomNavigationBar_iconTint, this.itemIconTint)
        itemIconTintActive = typedArray.getColor(R.styleable.CustomBottomNavigationBar_iconTintActive, this.itemIconTintActive)
        activeItem = typedArray.getInt(R.styleable.CustomBottomNavigationBar_activeItem, this.activeItem)
        itemFontFamily = typedArray.getResourceId(R.styleable.CustomBottomNavigationBar_itemFontFamily, this.itemFontFamily)
        itemAnimDuration = typedArray.getInt(R.styleable.CustomBottomNavigationBar_duration, this.itemAnimDuration.toInt()).toLong()
        items = BottomBarParser(context, typedArray.getResourceId(R.styleable.CustomBottomNavigationBar_menu, 0)).parse()
        typedArray.recycle()

        setBackgroundColor(barBackgroundColor)

        // Update default attribute values
        paintIndicator.color = barIndicatorColor
        paintText.color = itemTextColor
        paintText.textSize = itemTextSize

        if (itemFontFamily != 0)
            paintText.typeface = ResourcesCompat.getFont(context, itemFontFamily)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        var lastX = barSideMargins
        itemWidth = (width - barSideMargins * 2) / items.size

        for (item in items) {
            // Prevent text overflow by shortening the item title
            var shorted = false
            while (paintText.measureText(item.title) > itemWidth - itemIconSize - itemIconMargin - (barItemPadding*2)) {
                item.title = item.title.dropLast(1)
                shorted = true
            }

            // Set ellipsize mode if a text mode is shorted
            if (shorted) {
                item.title = item.title.dropLast(1)
                item.title += context.getString(R.string.ellipsis_attr)
            }

            item.rect = RectF(lastX, 0f, itemWidth + lastX, height.toFloat())
            lastX += itemWidth
        }

        // Initial first active item
        setActiveItem(activeItem)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val textHeight = (paintText.descent() + paintText.ascent()) / 2

        for ((i, item) in items.withIndex()) {
            val textLength = paintText.measureText(item.title)

            item.icon.mutate()
            item.icon.setBounds(item.rect.centerX().toInt() - itemIconSize.toInt() / 2 - ((textLength/2) * (1-(255 - item.alpha) / 255f)).toInt(),
                height / 2 - itemIconSize.toInt() / 2,
                item.rect.centerX().toInt() + itemIconSize.toInt() / 2 - ((textLength/2) * (1-(255 - item.alpha) / 255f)).toInt(),
                height / 2 + itemIconSize.toInt() / 2)

            this.paintText.alpha = item.alpha

            DrawableCompat.setTint(item.icon , if (i == activeItem) currentIconTint else itemIconTint)
            item.icon.draw(canvas)

            canvas.drawText(item.title, item.rect.centerX() + itemIconSize/2 + itemIconMargin,
                item.rect.centerY() - textHeight, paintText)
        }

        //indicator
        rect.left = indicatorLocation
        rect.top = items[activeItem].rect.centerY() - itemIconSize/2 - barItemPadding
        rect.right = indicatorLocation + itemWidth
        rect.bottom = items[activeItem].rect.centerY() + itemIconSize/2 + barItemPadding
        canvas.drawRoundRect(rect, 20f, 20f, paintIndicator)
    }

    // Touch events listener
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (event.action == MotionEvent.ACTION_UP && abs(event.downTime - event.eventTime) < 500)
            for ((itemId, item) in items.withIndex())
                if (item.rect.contains(event.x, event.y))
                    if (itemId != this.activeItem) {
                        setActiveItem(itemId)
                        onItemSelected(item)
                    } else
                        onItemReselected(itemId)

        return true
    }

    //Activate Item
    fun setActiveItem(pos: Int) {
        activeItem = pos

        animateIndicator(pos)

        for ((i, item) in items.withIndex()){
            animateAlpha(item, if (i == pos) 255 else 0)
        }


        animateIconTint()
    }

    // Animators

    private fun animateIconTint() {
        val animator = ValueAnimator.ofObject(ArgbEvaluator(), itemIconTint, itemIconTintActive)
        animator.duration = itemAnimDuration
        animator.addUpdateListener {
            currentIconTint = it.animatedValue as Int
        }

        animator.start()
    }

    private fun animateAlpha(item: BottomBarItem, to: Int) {
        val animator = ValueAnimator.ofInt(item.alpha, to)
        animator.duration = itemAnimDuration

        animator.addUpdateListener {
            val value = it.animatedValue as Int
            item.alpha = value
            invalidate()
        }

        animator.start()
    }

    private fun animateIndicator(pos: Int) {
        val animator = ValueAnimator.ofFloat(indicatorLocation, items[pos].rect.left)
        animator.duration = itemAnimDuration
        animator.interpolator = DecelerateInterpolator()

        animator.addUpdateListener { animation ->
            indicatorLocation = animation.animatedValue as Float
        }

        animator.start()
    }

}