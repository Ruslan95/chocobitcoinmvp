package com.newinstance.bitcoinchocomvp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.newinstance.bitcoinchocomvp.db.ConvertationHistoryDao
import com.newinstance.bitcoinchocomvp.mvp.model.ConvertationModel
import io.reactivex.Observable
import androidx.paging.LivePagedListBuilder
import androidx.paging.RxPagedListBuilder


/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
class ConvertatioHistoryViewModel():ViewModel(){
    var list:Observable<PagedList<ConvertationModel>>? = null
    fun init(dbDao:ConvertationHistoryDao){
        val pagedListConfig = PagedList.Config.Builder().setEnablePlaceholders(true)
            .setPageSize(20).build()
        list = RxPagedListBuilder(
            dbDao.allPaged(),
            pagedListConfig
        ).buildObservable()
    }

}
