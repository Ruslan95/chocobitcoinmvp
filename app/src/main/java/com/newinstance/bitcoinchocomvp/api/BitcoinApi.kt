package com.newinstance.bitcoinchocomvp.api

import com.google.gson.JsonObject
import com.newinstance.bitcoinchocomvp.BuildConfig
import com.newinstance.bitcoinchocomvp.mvp.model.BitcoinCurrencyModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */
interface BitcoinApi {
    companion object {
        const val BASE_URL = BuildConfig.SERVER_URL
    }

    @GET("/v1/bpi/currentprice/{currency_code}.json")
    fun getBtcCurrencyPrice(@Path("currency_code") currency_code: String): Observable<Response<BitcoinCurrencyModel>>

    @GET("/v1/bpi/historical/close.json")
    fun getBtcMonthHistorical(@Query("currency") currency_code: String): Observable<JsonObject>

    @GET("/v1/bpi/historical/close.json")
    fun getBtcCurrencyHistorical(
        @Query("currency") currency: String,
        @Query("start") start: String,
        @Query("end") end: String
    ): Observable<JsonObject>
}