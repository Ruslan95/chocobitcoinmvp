package com.newinstance.bitcoinchocomvp.api

import com.newinstance.bitcoinchocomvp.BuildConfig
import com.newinstance.bitcoinchocomvp.mvp.model.TransactionsModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
interface TransactionsApi {
    companion object {
        const val TRANSACTIONS_URL = BuildConfig.TRANSACTIONS_URL
    }

    @POST("/api/transactions")
    fun getTransactionsList(): Observable<Response<List<TransactionsModel>>>

}