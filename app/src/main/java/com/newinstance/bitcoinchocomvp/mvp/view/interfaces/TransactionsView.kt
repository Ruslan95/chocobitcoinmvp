package com.newinstance.bitcoinchocomvp.mvp.view.interfaces

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.newinstance.bitcoinchocomvp.mvp.model.TransactionsModel

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@StateStrategyType(value = SkipStrategy::class)
interface TransactionsView :MvpView{
    fun onLoading(isDone:Boolean)
    fun onLoaded(list:List<TransactionsModel>)
    fun onFailed(msg:String)
}