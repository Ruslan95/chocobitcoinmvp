package com.newinstance.bitcoinchocomvp.mvp.presenters

import android.content.Context
import android.net.ConnectivityManager
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.newinstance.bitcoinchocomvp.BitcoinApp
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */

open class BasePresenter <T: MvpView> : MvpPresenter<T>(){
    var disposables = CompositeDisposable()
    private val cx = BitcoinApp.appComponent.getContext()

    fun isOnline(): Boolean {
        val cm = (cx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?)!!
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    fun destroyDisposables() {
        disposables.dispose()
    }
}