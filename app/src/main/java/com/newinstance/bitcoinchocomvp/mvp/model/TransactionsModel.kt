package com.newinstance.bitcoinchocomvp.mvp.model

import com.google.gson.annotations.SerializedName
import com.newinstance.bitcoinchocomvp.utils.CommonUtils

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
/*
sealed class Transactions {
    companion object{
        const val SELL = 0
        const val BUY = 1
    }


    abstract val itemViewType:Int

    data class SellTrasaction (override val itemViewType:Int = SELL):Transactions()
    data class BuyTransaction (override val itemViewType:Int = BUY):Transactions()
}
*/
data class TransactionsModel(
    @SerializedName("date")
    val date: String = "",
    @SerializedName("amount")
    val amount: String = "",
    @SerializedName("price")
    val price: String = "",
    @SerializedName("type")
    val type: Int = 0,
    @SerializedName("tid")
    val tid: Int = 0
)

