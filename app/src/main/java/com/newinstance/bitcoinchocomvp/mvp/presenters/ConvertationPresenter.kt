package com.newinstance.bitcoinchocomvp.mvp.presenters

import android.util.Log
import androidx.lifecycle.ViewModel
import com.arellomobile.mvp.InjectViewState
import com.newinstance.bitcoinchocomvp.BitcoinApp
import com.newinstance.bitcoinchocomvp.R
import com.newinstance.bitcoinchocomvp.db.LocalDataSource
import com.newinstance.bitcoinchocomvp.di.DaggerSComponent
import com.newinstance.bitcoinchocomvp.mvp.view.interfaces.ConvertationHistoryView
import com.newinstance.bitcoinchocomvp.viewmodel.ConvertatioHistoryViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_conertaion_information.view.*
import java.text.NumberFormat
import javax.inject.Inject

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@InjectViewState
class ConvertationPresenter @Inject constructor(): BasePresenter<ConvertationHistoryView>(){
    init {
        DaggerSComponent.builder().appComponent(BitcoinApp.getApplicationComponent())
            .build().inject(this)
    }

    @Inject lateinit var ds:LocalDataSource

    fun loadHistory(viewModel: ConvertatioHistoryViewModel){
        viewState.onLoading(false)
        viewModel.init(ds.dbDao)
        disposables.add(
            viewModel.list?.
                subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())?.subscribe {
                    viewState.onLoading(true)
                    if(!it.isNullOrEmpty()){
                        viewState.onLoaded(it)
                    }else{
                        viewState.onEmpty()
                    }
                }!!
        )
    }
}