package com.newinstance.bitcoinchocomvp.mvp.view.interfaces

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.newinstance.bitcoinchocomvp.mvp.model.BitcoinCurrencyModel

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@StateStrategyType(value = SkipStrategy::class)
interface ConverterView:MvpView {
    fun onLoading(isDone:Boolean)
    fun onLoadedCurrencyValue(model: BitcoinCurrencyModel)
    fun onFail(msg: String?)
}