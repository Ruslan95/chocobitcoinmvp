package com.newinstance.bitcoinchocomvp.mvp.presenters

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.github.mikephil.charting.data.Entry
import com.google.gson.JsonObject
import com.newinstance.bitcoinchocomvp.BitcoinApp
import com.newinstance.bitcoinchocomvp.api.BitcoinApi
import com.newinstance.bitcoinchocomvp.di.DaggerSComponent
import com.newinstance.bitcoinchocomvp.mvp.view.interfaces.RateView
import com.newinstance.bitcoinchocomvp.utils.CommonUtils
import com.newinstance.bitcoinchocomvp.utils.Constants
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@InjectViewState
class RatePresenter @Inject constructor() : BasePresenter<RateView>() {

    init {
        DaggerSComponent.builder()
            .appComponent(BitcoinApp.getApplicationComponent())
            .build().inject(this)
    }

    @Inject
    lateinit var apiService: BitcoinApi

    fun loadBitcoinValues(currency_code:String){
        if(!isOnline()){
            viewState.onFail("Нет интернет соединия")
        }else{
            viewState.onLoading(false)
            disposables.add(
                apiService.getBtcMonthHistorical(currency_code)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({
                        val bpi = it.get("bpi") as JsonObject
                        val jsonValue = JSONObject(bpi.toString())
                        val iterator = jsonValue.keys()

                        val entry  = ArrayList<Entry>()
                        while (iterator.hasNext()){
                            val key = iterator.next()
                            try {
                                val value = jsonValue.get(key)
                                val date = CommonUtils.formatISO_8601_typed(time = key,datyType =
                                Calendar.DAY_OF_MONTH)
                                Log.wtf("Values","$value , ${date.toFloat()}")
                                entry.add(Entry(date.toFloat(), value.toString().toFloat()))
                                //x += 1
                            } catch (e: JSONException) {
                                // Something went wrong!
                            }
                        }
                        viewState.onLoading(true)
                        viewState.onLoadChart(entry)
                    },{
                        viewState.onLoading(true)
                        viewState.onFail(it.message.toString())
                    })
            )
        }
    }

    fun getDateHistorical(currency_code:String,mode:String){
        if(!isOnline()){
            viewState.onFail("Нет интернет соединия")
        }else{
            viewState.onLoading(false)
            val cal = Calendar.getInstance()
            val dateFormat = SimpleDateFormat("yyyy-MM-dd")
            var start = ""
            var end = ""
            when(mode){
                "week" -> {
                    cal.add(Calendar.WEEK_OF_MONTH,-1)
                    start = dateFormat.format(cal.time)
                    cal.add(Calendar.WEEK_OF_MONTH,1)
                    end = dateFormat.format(cal.time)
                }
                "year" -> {
                    cal.add(Calendar.YEAR,-1)
                    start = dateFormat.format(cal.time)
                    cal.add(Calendar.YEAR,1)
                    end = dateFormat.format(cal.time)
                }
            }
            disposables.add(
                apiService.getBtcCurrencyHistorical(currency_code,start = start,end = end)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({
                        val bpi = it.get("bpi") as JsonObject
                        val jsonValue = JSONObject(bpi.toString())
                        val iterator = jsonValue.keys()

                        val entry  = ArrayList<Entry>()
                        while (iterator.hasNext()){
                            val key = iterator.next()
                            try {
                                val value = jsonValue.get(key)
                                var date = 0
                                when (mode){
                                    "week" -> {
                                        date =  CommonUtils.formatISO_8601_typed(time = key,datyType =
                                        Calendar.DAY_OF_WEEK)
                                    }
                                    "year" -> {
                                        date =  CommonUtils.formatISO_8601_typed(time = key,datyType =
                                        Calendar.MONTH)
                                    }
                                }
                                Log.wtf("Values","$value , ${date.toFloat()}")
                                entry.add(Entry(date.toFloat(), value.toString().toFloat()))
                                //x += 1
                            } catch (e: JSONException) {
                                // Something went wrong!
                            }
                        }
                        viewState.onLoading(true)
                        viewState.onLoadChart(entry)
                    },{
                        viewState.onLoading(true)
                        viewState.onFail(it.message.toString())
                    })
            )
        }
    }

    fun loadCurrencyValues(currencyCode:String = "KZT"){
        viewState.onLoading(false)
        if(isOnline()){
            disposables.add(
                apiService.getBtcCurrencyPrice(currencyCode)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({
                        viewState.onLoading(true)
                        if(it.isSuccessful){
                            viewState.onLoadData(it.body()!!)
                        }else{
                            viewState.onFail(it.message())
                        }

                    },{
                        viewState.onLoading(true)
                        viewState.onFail(it.message.toString())
                    })
            )
        }else{
            viewState.onLoading(true)
            viewState.onFail("Не подключения к интернету")
        }
    }




}