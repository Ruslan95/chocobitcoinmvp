package com.newinstance.bitcoinchocomvp.mvp.view.fragments


import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet

import com.newinstance.bitcoinchocomvp.R
import com.newinstance.bitcoinchocomvp.mvp.model.BitcoinCurrencyModel
import com.newinstance.bitcoinchocomvp.mvp.presenters.RatePresenter
import com.newinstance.bitcoinchocomvp.mvp.view.interfaces.RateView
import com.newinstance.bitcoinchocomvp.ui.LottieLoaderDialog
import com.newinstance.bitcoinchocomvp.utils.UserPref
import kotlinx.android.synthetic.main.fragment_converter.*
import kotlinx.android.synthetic.main.fragment_rate.*
import org.jetbrains.anko.support.v4.toast
import java.lang.NumberFormatException
import java.text.NumberFormat

/**
 * A simple [Fragment] subclass.
 */
class RateFragment : MvpAppCompatFragment(),RateView,View.OnClickListener {

    @InjectPresenter
    lateinit var presenter:RatePresenter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rate, container, false)
    }

    private var lottieLoaderDialog = LottieLoaderDialog()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListeners()
        presenter.loadBitcoinValues(UserPref().currency)
        presenter.loadCurrencyValues(UserPref().currency)
    }

    override fun onLoadChart(list: List<Entry>){
        val dataSet = LineDataSet(list, "")
        val lineData = LineData(dataSet)

        line_char.description.text = ""
        line_char.data = lineData
        line_char.invalidate() // refresh
    }

    override fun onDestroy() {
        super.onDestroy()
        //presenter.disposables.clear()
    }


    override fun onLoadData(model: BitcoinCurrencyModel){
        var amount = 0f
        when(UserPref().currency){
            "EUR" -> {
                amount = model.bpi.eur.rateFloat
            }
            "KZT" -> {
                amount = model.bpi.kzt.rateFloat
            }
            "USD" -> {
                amount = model.bpi.usd.rateFloat
            }
        }
        dialog_conv_amount.text = NumberFormat.getInstance().format(amount)
    }

    override fun onFail(msg:String){
        toast(msg)
    }

    override fun onLoading(isDone:Boolean){
        if(!isDone){
            lottieLoaderDialog.show(childFragmentManager,"")
        }else{
            lottieLoaderDialog.dismiss()
        }
    }

    private fun initClickListeners(){
        cardView_eur.setOnClickListener(this)
        cardView_kzt.setOnClickListener(this)
        card_usd.setOnClickListener(this)
        text_week.setOnClickListener(this)
        text_month.setOnClickListener(this)
        text_year.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.cardView_eur -> {
                UserPref().apply{
                    currency = "EUR"
                }
                initPref()
                presenter.loadCurrencyValues(UserPref().currency)
            }
            R.id.cardView_kzt -> {
                UserPref().apply{
                    currency = "KZT"
                }
                initPref()
                presenter.loadCurrencyValues(UserPref().currency)
            }
            R.id.card_usd -> {
                UserPref().apply{
                    currency = "USD"
                }
                initPref()
                presenter.loadCurrencyValues(UserPref().currency)
            }
            R.id.text_month -> {
                text_month.setTextColor(Color.BLUE)
                text_month.setBackgroundResource(R.drawable.rounded_shape_white)
                text_week.setTextColor(Color.BLACK)
                text_week.setBackgroundResource(R.drawable.rounded_shape_blue)
                text_year.setTextColor(Color.BLACK)
                text_year.setBackgroundResource(R.drawable.rounded_shape_blue)
                presenter.loadBitcoinValues(UserPref().currency)
            }
            R.id.text_week -> {
                text_week.setTextColor(Color.BLUE)
                text_week.setBackgroundResource(R.drawable.rounded_shape_white)
                text_month.setTextColor(Color.BLACK)
                text_month.setBackgroundResource(R.drawable.rounded_shape_blue)
                text_year.setTextColor(Color.BLACK)
                text_year.setBackgroundResource(R.drawable.rounded_shape_blue)
                presenter.getDateHistorical(UserPref().currency,"week")
            }
            R.id.text_year -> {
                text_year.setTextColor(Color.BLUE)
                text_year.setBackgroundResource(R.drawable.rounded_shape_white)
                text_week.setTextColor(Color.BLACK)
                text_week.setBackgroundResource(R.drawable.rounded_shape_blue)
                text_month.setTextColor(Color.BLACK)
                text_month.setBackgroundResource(R.drawable.rounded_shape_blue)
                presenter.getDateHistorical(UserPref().currency,"year")
            }
        }
    }

    private fun initPref(){
        when(UserPref().currency){
            "EUR" -> {
                openUi(cardView_eur)
                blockUi(cardView_kzt)
                blockUi(card_usd)
                UserPref().apply{
                    currency = "EUR"
                }
            }
            "KZT" -> {
                openUi(cardView_kzt)
                blockUi(card_usd)
                blockUi(cardView_eur)
                UserPref().apply{
                    currency = "KZT"
                }
            }
            "USD" -> {
                blockUi(cardView_eur)
                blockUi(cardView_kzt)
                openUi(card_usd)
                UserPref().apply{
                    currency = "USD"
                }
            }
        }
    }

    private fun blockUi(cardView: CardView){
        cardView.setCardBackgroundColor(Color.parseColor("#ff33b5e5"))
        val scaleDown = ObjectAnimator.ofPropertyValuesHolder(
            cardView,
            PropertyValuesHolder.ofFloat("scaleX", 1.0f),
            PropertyValuesHolder.ofFloat("scaleY", 1.0f)
        )
        scaleDown.duration = 500
        scaleDown.start()
    }
    private fun openUi(cardView: CardView){
        cardView.setCardBackgroundColor(Color.WHITE)
        val scaleDown = ObjectAnimator.ofPropertyValuesHolder(
            cardView,
            PropertyValuesHolder.ofFloat("scaleX", 1.5f),
            PropertyValuesHolder.ofFloat("scaleY", 1.5f)
        )
        scaleDown.duration = 500
        scaleDown.start()
    }
}
