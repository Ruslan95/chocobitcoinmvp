package com.newinstance.bitcoinchocomvp.mvp.view.interfaces

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.github.mikephil.charting.data.Entry
import com.newinstance.bitcoinchocomvp.mvp.model.BitcoinCurrencyModel

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@StateStrategyType(value = SkipStrategy::class)
interface RateView : MvpView {
    fun onLoadData(result: BitcoinCurrencyModel)
    fun onFail(msg:String)
    fun onLoadChart(list: List<Entry>)
    fun onLoading(isDone:Boolean)
    //fun onLoadChartEntry(listEntry: ArrayList<Entry>)
}