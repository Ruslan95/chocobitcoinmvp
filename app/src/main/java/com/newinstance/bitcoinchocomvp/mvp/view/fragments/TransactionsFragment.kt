package com.newinstance.bitcoinchocomvp.mvp.view.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter

import com.newinstance.bitcoinchocomvp.R
import com.newinstance.bitcoinchocomvp.adapters.appAdapters.TransactionsAdapter
import com.newinstance.bitcoinchocomvp.mvp.model.TransactionsModel
import com.newinstance.bitcoinchocomvp.mvp.presenters.TransactionsPresenter
import com.newinstance.bitcoinchocomvp.mvp.view.interfaces.TransactionsView
import com.newinstance.bitcoinchocomvp.ui.LottieLoaderDialog
import kotlinx.android.synthetic.main.fragment_transactions.*
import org.jetbrains.anko.support.v4.toast

/**
 * A simple [Fragment] subclass.
 */
class TransactionsFragment : MvpAppCompatFragment(),TransactionsView {

    private lateinit var adapter: TransactionsAdapter
    private var lottieLoaderDialog= LottieLoaderDialog()

    @InjectPresenter
    lateinit var presenter:TransactionsPresenter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transactions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = TransactionsAdapter()
        transactions_recycler.layoutManager = LinearLayoutManager(activity)
        transactions_recycler.adapter = adapter
        presenter.loadTransactions()


        swipeRefereshLayout.setOnRefreshListener {
            presenter.loadTransactions()
        }
    }

    override fun onLoading(isDone: Boolean) {
        if(isDone){
            lottieLoaderDialog?.dismiss()
        }else{
            lottieLoaderDialog?.show(childFragmentManager,"")
        }
        swipeRefereshLayout.isRefreshing = !isDone
    }

    override fun onLoaded(list: List<TransactionsModel>) {
        adapter.setList(list)
    }

    override fun onFailed(msg: String) {
        toast(msg)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.disposables.clear()
    }


}
