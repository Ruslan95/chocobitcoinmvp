package com.newinstance.bitcoinchocomvp.mvp.view.fragments


import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.BounceInterpolator
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.newinstance.bitcoinchocomvp.BitcoinApp

import com.newinstance.bitcoinchocomvp.R
import com.newinstance.bitcoinchocomvp.db.LocalDataSource
import com.newinstance.bitcoinchocomvp.di.DaggerSComponent
import com.newinstance.bitcoinchocomvp.mvp.model.BitcoinCurrencyModel
import com.newinstance.bitcoinchocomvp.mvp.model.ConvertationModel
import com.newinstance.bitcoinchocomvp.mvp.presenters.ConverterPresenter
import com.newinstance.bitcoinchocomvp.mvp.view.interfaces.ConverterView
import com.newinstance.bitcoinchocomvp.mvp.view.dialogs.ConvertationInfoDialog
import com.newinstance.bitcoinchocomvp.ui.LottieLoaderDialog
import com.newinstance.bitcoinchocomvp.utils.UserPref
import kotlinx.android.synthetic.main.fragment_converter.*
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject



/**
 * A simple [Fragment] subclass.
 */
class ConverterFragment : MvpAppCompatFragment(), ConverterView,View.OnClickListener {

    override fun onLoading(isDone: Boolean) {
        if(isDone){
            lottieLoaderDialog?.let {
                lottieLoaderDialog.dismiss()
            }
        }
    }

    init {
        DaggerSComponent.builder().appComponent(BitcoinApp.getApplicationComponent()).build().inject(this)
    }

    @InjectPresenter
    lateinit var presenter: ConverterPresenter

    @Inject lateinit var ds:LocalDataSource

    private var lottieLoaderDialog= LottieLoaderDialog()

    private var currentCurrency = "KZT"
    private var animator: ObjectAnimator? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_converter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListeners()
        initPref()
    }

    private fun initPref(){
        when(UserPref().currency){
            "EUR" -> {
                openUi(cardView)
                blockUi(cardView2)
                blockUi(cardView3)
                UserPref().apply{
                    currency = "EUR"
                }
            }
            "KZT" -> {
                openUi(cardView2)
                blockUi(cardView)
                blockUi(cardView3)
                UserPref().apply{
                    currency = "KZT"
                }
            }
            "USD" -> {
                blockUi(cardView)
                blockUi(cardView2)
                openUi(cardView3)
                UserPref().apply{
                    currency = "USD"
                }
            }
        }
    }

    private fun blockUi(cardView: CardView){
        cardView.setCardBackgroundColor(Color.parseColor("#ff33b5e5"))
        val scaleDown = ObjectAnimator.ofPropertyValuesHolder(
            cardView,
            PropertyValuesHolder.ofFloat("scaleX", 1.0f),
            PropertyValuesHolder.ofFloat("scaleY", 1.0f)
        )
        scaleDown.duration = 500
        scaleDown.start()
    }

    private fun openUi(cardView: CardView){
        cardView.setCardBackgroundColor(Color.WHITE)
        val scaleDown = ObjectAnimator.ofPropertyValuesHolder(
            cardView,
            PropertyValuesHolder.ofFloat("scaleX", 1.5f),
            PropertyValuesHolder.ofFloat("scaleY", 1.5f)
        )
        scaleDown.duration = 500
        scaleDown.start()
    }

    private fun initClickListeners(){
        materialButtonConv.setOnClickListener(this)
        materialButtonHistory.setOnClickListener(this)
        cardView.setOnClickListener(this)
        cardView2.setOnClickListener(this)
        cardView3.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.materialButtonConv -> {
                if(edit_text_amount.text.isNotEmpty() &&
                    edit_text_amount.text.toString().toFloat() != 0f){
                    lottieLoaderDialog.show(childFragmentManager,"loader_1")
                    presenter.loadCurrencyValues(UserPref().currency)
                }else{
                    toast("Введите правильное значение")
                }
            }
            R.id.materialButtonHistory -> {
                ConvertationInfoDialog().show(childFragmentManager,"conv")
            }
            R.id.cardView -> {
                UserPref().apply{
                    currency = "EUR"
                }
                initPref()
            }
            R.id.cardView2 -> {
                UserPref().apply{
                    currency = "KZT"
                }
                initPref()
            }
            R.id.cardView3 -> {
                UserPref().apply{
                    currency = "USD"
                }
                initPref()
            }
        }
    }

    override fun onLoadedCurrencyValue(model: BitcoinCurrencyModel) {
        calcValuses(model)
    }

    private fun calcValuses(model: BitcoinCurrencyModel){
        var amount = 0f
        when(UserPref().currency){
            "EUR" -> {
                amount = model.bpi.eur.rateFloat
            }
            "KZT" -> {
                amount = model.bpi.kzt.rateFloat
            }
            "USD" -> {
                amount = model.bpi.usd.rateFloat
            }
        }
        var btc_amount = edit_text_amount.text.toString().toFloat()
        val totalAmount = btc_amount * amount

        ds.insert(ConvertationModel(totalAmount,UserPref().currency,btc_amount,model.time.toString()))
        ConvertationInfoDialog().show(childFragmentManager,"conv")
    }

    override fun onFail(msg: String?) {
        toast(msg!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.disposables.clear()
    }
}
