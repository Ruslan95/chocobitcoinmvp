package com.newinstance.bitcoinchocomvp.mvp.presenters

import androidx.core.graphics.BitmapCompat
import com.arellomobile.mvp.InjectViewState
import com.newinstance.bitcoinchocomvp.BitcoinApp
import com.newinstance.bitcoinchocomvp.api.BitcoinApi
import com.newinstance.bitcoinchocomvp.di.AppModule
import com.newinstance.bitcoinchocomvp.di.DaggerSComponent
import com.newinstance.bitcoinchocomvp.mvp.view.interfaces.ConverterView
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@InjectViewState
class ConverterPresenter @Inject constructor() : BasePresenter<ConverterView>() {

    init {
        DaggerSComponent.builder().appComponent(BitcoinApp.getApplicationComponent())
            .build().inject(this)
    }

    @Inject @Named("provideBitcoinRetrofit")
    lateinit var apiService: BitcoinApi

    fun loadCurrencyValues(currencyCode:String = "KZT"){
        viewState.onLoading(false)
        if(isOnline()){
            disposables.add(
                apiService.getBtcCurrencyPrice(currencyCode)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({
                        viewState.onLoading(true)
                        if(it.isSuccessful){
                            viewState.onLoadedCurrencyValue(it.body()!!)
                        }else{
                            viewState.onFail(it.message())
                        }

                    },{
                        viewState.onLoading(true)
                        viewState.onFail(it.message.toString())
                    })
            )
        }else{
            viewState.onLoading(true)
            viewState.onFail("Не подключения к интернету")
        }
    }

}