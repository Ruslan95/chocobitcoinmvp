package com.newinstance.bitcoinchocomvp.mvp.presenters

import com.arellomobile.mvp.InjectViewState
import com.newinstance.bitcoinchocomvp.BitcoinApp
import com.newinstance.bitcoinchocomvp.api.TransactionsApi
import com.newinstance.bitcoinchocomvp.di.DaggerSComponent
import com.newinstance.bitcoinchocomvp.mvp.view.interfaces.TransactionsView
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@InjectViewState
class TransactionsPresenter @Inject constructor():BasePresenter<TransactionsView>(){
    init {
        DaggerSComponent.builder().appComponent(BitcoinApp.appComponent).build().inject(this)
    }

    @Inject lateinit var apiService:TransactionsApi

    fun loadTransactions(){
        viewState.onLoading(isDone = false)
        disposables.add(
            apiService.getTransactionsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    viewState.onLoading(isDone = true)
                    if(it.isSuccessful){
                        viewState.onLoaded(it.body()!!)
                    }else{
                        viewState.onFailed(it.message())
                    }
                },{
                    viewState.onLoading(isDone = true)
                    viewState.onFailed(it.message.toString())
                })
        )
    }
}