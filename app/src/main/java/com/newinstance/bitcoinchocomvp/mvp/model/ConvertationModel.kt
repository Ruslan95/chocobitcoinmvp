package com.newinstance.bitcoinchocomvp.mvp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@Entity(tableName = "convertation_table")
open class ConvertationModel{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int? = null

    @ColumnInfo(name = "amount")
    var amount:Float? = null

    @ColumnInfo(name = "currency")
    var currency:String? = null

    @ColumnInfo(name = "btc_amount")
    var btc_amount:Float? = null

    @ColumnInfo(name = "date")
    var date:String? = null

    /**
     * Constructor for manually added conv_model
     *
     */
    constructor(amount:Float?,currency:String?,btc_amount: Float?, date: String?) {
        this.amount = amount
        this.currency = currency
        this.btc_amount = btc_amount
        this.date = date
    }
}
