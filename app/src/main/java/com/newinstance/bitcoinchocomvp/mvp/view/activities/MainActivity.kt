package com.newinstance.bitcoinchocomvp.mvp.view.activities

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.arellomobile.mvp.MvpAppCompatActivity
import com.newinstance.bitcoinchocomvp.R
import com.newinstance.bitcoinchocomvp.mvp.view.fragments.ConverterFragment
import com.newinstance.bitcoinchocomvp.mvp.view.fragments.RateFragment
import com.newinstance.bitcoinchocomvp.mvp.view.fragments.TransactionsFragment
import kotlinx.android.synthetic.main.activity_main.*





/**
 * Created by Ruslan Erdenoff on 08.11.2019.
 */

class MainActivity : MvpAppCompatActivity() {

    private val fragmentConverter = ConverterFragment()
    private val fragmentRate = RateFragment()
    private val fragmentTransactions = TransactionsFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottom_nav_bar.onItemSelected = {
            when(it.id){
                R.id.item_main -> {
                    changeFragment(fragmentRate)
                }
                R.id.item_transaction -> {
                    changeFragment(fragmentTransactions)
                }
                R.id.item_converter -> {
                    changeFragment(fragmentConverter)
                }
            }
        }
        changeFragment(fragmentRate)
    }

    private fun changeFragment(fragment:Fragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.content, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    
}
