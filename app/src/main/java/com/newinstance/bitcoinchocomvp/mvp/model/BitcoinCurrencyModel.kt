package com.newinstance.bitcoinchocomvp.mvp.model

import com.google.gson.annotations.SerializedName
import com.newinstance.bitcoinchocomvp.utils.CommonUtils

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */

data class BitcoinCurrencyModel(
    @SerializedName("bpi")
    val bpi: Bpi,
    @SerializedName("time")
    val time: Time,
    @SerializedName("disclaimer")
    val disclaimer: String = ""
)

data class Bpi(
    @SerializedName("KZT")
    val kzt: KZT,
    @SerializedName("USD")
    val usd: USD,
    @SerializedName("EUR")
    val eur: EUR
)

data class KZT(
    @SerializedName("rate_float")
        val rateFloat: Float = 0f,
    @SerializedName("code")
    val code: String = "",
    @SerializedName("rate")
    val rate: String = "",
    @SerializedName("description")
    val description: String = ""
)


data class USD(
    @SerializedName("rate_float")
    val rateFloat: Float = 0f,
    @SerializedName("code")
    val code: String = "",
    @SerializedName("rate")
    val rate: String = "",
    @SerializedName("description")
    val description: String = ""
)

data class EUR(
    @SerializedName("rate_float")
    val rateFloat: Float = 0f,
    @SerializedName("code")
    val code: String = "",
    @SerializedName("rate")
    val rate: String = "",
    @SerializedName("description")
    val description: String = ""
)


data class Time(
    @SerializedName("updateduk")
    val updateduk: String = "",
    @SerializedName("updatedISO")
    val updatedISO: String = "",
    @SerializedName("updated")
    val updated: String = ""
){
    override fun toString(): String {
        val date = CommonUtils.formatISO_8601(time = updatedISO)
        return date
    }
}