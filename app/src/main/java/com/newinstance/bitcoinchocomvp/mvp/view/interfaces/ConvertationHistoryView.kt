package com.newinstance.bitcoinchocomvp.mvp.view.interfaces

import androidx.paging.PagedList
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.newinstance.bitcoinchocomvp.mvp.model.ConvertationModel
import io.reactivex.Observable

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
@StateStrategyType(value = SkipStrategy::class)
interface ConvertationHistoryView : MvpView {
    fun onLoading(isDone:Boolean)
    fun onLoaded(list:PagedList<ConvertationModel>)
    fun onFail(msg:String)
    fun onEmpty()
}