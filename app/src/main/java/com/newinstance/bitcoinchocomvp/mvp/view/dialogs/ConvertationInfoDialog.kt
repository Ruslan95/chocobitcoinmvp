package com.newinstance.bitcoinchocomvp.mvp.view.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.newinstance.bitcoinchocomvp.R
import com.newinstance.bitcoinchocomvp.adapters.appAdapters.ConvertationsAdapter
import com.newinstance.bitcoinchocomvp.mvp.model.ConvertationModel
import com.newinstance.bitcoinchocomvp.mvp.presenters.ConvertationPresenter
import com.newinstance.bitcoinchocomvp.mvp.view.interfaces.ConvertationHistoryView
import com.newinstance.bitcoinchocomvp.ui.LottieLoaderDialog
import com.newinstance.bitcoinchocomvp.ui.RoundedCommonBottomSheetDialogFragment
import com.newinstance.bitcoinchocomvp.viewmodel.ConvertatioHistoryViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_conertaion_information.*
import kotlinx.android.synthetic.main.dialog_conertaion_information.view.*
import org.jetbrains.anko.support.v4.toast
import java.text.NumberFormat

/**
 * Created by Ruslan Erdenoff on 09.11.2019.
 */
class ConvertationInfoDialog: RoundedCommonBottomSheetDialogFragment(),ConvertationHistoryView {

    override fun onEmpty() {
        dialogClientRecyclerView.visibility = View.GONE
    }

    override fun onLoading(isDone: Boolean) {
        if(isDone){
            childFragmentManager.findFragmentByTag("loader")?.let {
                lottieLoader.dismiss()
            }
        }
    }

    override fun onLoaded(list: PagedList<ConvertationModel>) {
            adapter.submitList(list)
            val lastC = list.last()
            dialog_conv_currency.text = lastC.currency
            dialog_conv_amount.text = NumberFormat.getInstance().format(lastC.amount)
            when(lastC.currency) {
                "EUR" -> {
                    dialog_conv_flag.setImageResource(R.drawable.ic_european_flag)
                }
                "KZT" -> {
                    dialog_conv_flag.setImageResource(R.drawable.ic_kazakhstan_flag)
                }
                "USD" -> {
                    dialog_conv_flag.setImageResource(R.drawable.ic_usa_flag)
                }
            }
    }

    override fun onFail(msg:String) {
        toast(msg)
    }

    @InjectPresenter
    lateinit var presenter: ConvertationPresenter


    private lateinit var viewModel: ConvertatioHistoryViewModel
    private lateinit var lottieLoader:LottieLoaderDialog

    private val disposable = CompositeDisposable()

    private lateinit var adapter: ConvertationsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return inflater.inflate(R.layout.dialog_conertaion_information, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ConvertationsAdapter()
        viewModel = ViewModelProviders.of(activity!!).get(ConvertatioHistoryViewModel::class.java)
        view.dialogClientRecyclerView.layoutManager = LinearLayoutManager(activity!!)
        view.dialogClientRecyclerView.adapter = adapter
        lottieLoader = LottieLoaderDialog()
        lottieLoader.show(childFragmentManager,"loader")
        presenter.loadHistory(viewModel)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.disposables.clear()
    }
}
